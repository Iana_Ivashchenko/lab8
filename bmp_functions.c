#include "bmp_functions.h"

size_t calculate_padding(int img_width)
{
    size_t padding = 4 - (img_width * 3) % 4;
    return padding == 4 ? 0 : padding;
}


enum read_status readBMP(FILE* f, struct BMP * image)
{
    image->header = (struct bmp_head *)malloc(sizeof(struct bmp_head));
    fread(image->header, sizeof(struct bmp_head), 1, f);
    if (image->header->type != 0x424D && image->header->type != 0x4D42) return READ_TYPE_ERR;
    if (image->header->reserved != 0) return READ_RESERVED_ERR;
    if (image->header->bitCount != 24) return READ_UNSUPPORTED_FORMAT;

    image->width = image->header->width;
    image->height = image->header->height;

    image->size = image->width * image->height;
    image->pxArray = (struct pixel *)malloc(sizeof(struct pixel) * image->size);

    int8_t tr[4] = {0};
    size_t padding = calculate_padding(image->width);
    fseek(f, image->header->offBits, SEEK_SET);
    for (int i = 0; i < image->height; i++)
    {
        if(!fread(&image->pxArray[i*image->width], sizeof(struct pixel), image->width, f)) return READ_ERR;
        if (padding != 0)
        {
            fread(tr, sizeof(int8_t), padding, f);
        }
    }
    return READ_OK;
}

enum write_status writeBMP(struct BMP image, FILE* out)
{
    int8_t tr[4] = {0};
    const size_t pad = calculate_padding(image.width);
    if (!fwrite(image.header, sizeof(struct bmp_head), 1, out)){ printf("hdr"); return WRITE_ERROR;}
    for (int i = 0; i < image.height; i++)
    {
        if (!fwrite(&image.pxArray[i*image.width], sizeof(struct pixel), image.width, out)) { printf("data"); return WRITE_ERROR;}
        if (pad != 0)
        {
            fwrite(tr, sizeof(int8_t), pad, out);
        }
    }
    return WRITE_OK;
}

void set_pixel(const struct BMP* img, uint64_t x, uint64_t y, struct pixel p) 
{
    img->pxArray[y * img->width + x] = p;
}

struct pixel get_pixel(const struct BMP img, uint64_t x, uint64_t y) 
{
    return img.pxArray[y * img.width + x];
}