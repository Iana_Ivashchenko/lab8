lab8:
	gcc -g -c status_codes.c -o status_codes.o
	gcc -g -c bmp_functions.c -o bmp_functions.o
	gcc -g -c sepia.c -o sepia.o
	nasm -felf64 -g sepia_asm.asm -o sepia_asm.o
	gcc -g -c main.c -o main.o
	gcc -g *.o -o main

clean:
	rm -rf *.o
