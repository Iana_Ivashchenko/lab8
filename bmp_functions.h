#ifndef BMP_FUNCTIONS_H
#define BMP_FUNCTIONS_H
#define M_PI 3.14159265358979323846

#include <stdio.h>
#include <malloc.h>
#include <math.h>
#include <stdint.h>
#include "status_codes.h"



struct __attribute__((packed)) pixel
{
    unsigned char r;
    unsigned char g;
    unsigned char b;
};

struct BMP
{
    int width;
    int height;
    struct bmp_head * header;
    struct pixel * pxArray;
    int size;
};

struct __attribute__((packed)) bmp_head
{
    uint16_t type;
    uint32_t fileSize;
    uint32_t reserved;
    uint32_t offBits;
    uint32_t size;
    uint32_t width;
    uint32_t height;
    uint16_t planes;
    uint16_t bitCount;
    uint32_t compression;
    uint32_t sizeImage;
    uint32_t xPeels;
    uint32_t yPels;
    uint32_t clrUsed;
    uint32_t clrImportant;
};


enum write_status writeBMP(struct BMP image, FILE* out);
enum read_status readBMP(FILE* f, struct BMP* img);
size_t calculate_padding(int img_width);

void set_pixel(const struct BMP* img, uint64_t x, uint64_t y, struct pixel p);
struct pixel get_pixel(const struct BMP img, uint64_t x, uint64_t y);


#endif // BMP_FUNCTIONS_H
